# Énigmes scientifiques

Ce dépôt regroupe plusieurs énigmes scientifiques réalisées dans le cadre du club sciences et technologies du lycée Rotrou (Dreux).

## Avant de commencer

Pour chaque énigme vous disposez :
* d'un QR-code qui donne accès à un cadenas virtuel ;
* d'indices pour trouver comment dévérouiller le cadenas.

Les énigmes peuvent parraîtrent vagues ou difficiles au premier abord mais ont été conçues et testées pour être accessibles à des élèves dès la seconde. Il n'est pas nécessaire d'avoir des connaissances scientifiques avancées pour les résoudre (même si ça peut aider) et il est possible de les résoudre à plusieurs ou en utilisant des ressources externes (manuels, sites web, etc).

Bon courage !

PS : Nous déclinons toute responsabilité en cas de surchauffe des méninges.